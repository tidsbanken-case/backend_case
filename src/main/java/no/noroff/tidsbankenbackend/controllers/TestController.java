package no.noroff.tidsbankenbackend.controllers;


import no.noroff.tidsbankenbackend.model.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;


@RestController
public class TestController {

    private final String message;

    public TestController( @Value("${message}") String message) {
        this.message = message;
    }


    @GetMapping("test")
    public ResponseEntity<List<Test>> getAll() {
        List<Test> tests = Arrays.asList(
                new Test(1, "Think about vacation"),
                new Test(2, "Order vacation!"),
                new Test(3, "Vacation denied")
        );
        return ResponseEntity.ok(tests);
    }

    @GetMapping("env")
    public ResponseEntity getEnv() {
        return ResponseEntity.ok(message);
    }
}
